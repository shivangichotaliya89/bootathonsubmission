function DynamicTable():void
{
    var input:HTMLInputElement=<HTMLInputElement>document.getElementById("num");     //get the value from text box
    var table:HTMLTableElement=<HTMLTableElement>document.getElementById("table");      
    let msg :HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("msg"); //use for message


    var count:number=1;
    var num:number= +input.value;   //+is a used for  shortcut of an parseFloat function

    while(table.rows.length >0 )       //we have to delete all the rows of previus result first
    {
        
        if(table.rows.length==1)    //if only 1 row is left then it will be deleted using index 0
            table.deleteRow(0);
        else
          table.deleteRow(1);
    }
  
   
   
    if(input.value=="")     //check weather number empty or not
        alert("Please Enter The Number...!");
    else if(!Number.isInteger(num))       //check weather the input is integer or not
        alert("Please enter integer value....!"); 
    else if(num>0)                   //checking for positive integers
    {
        msg.innerHTML="<b>Multiplication Table For "+ num + " is As Follow</b>";
            for(count=1;count <= num;count++)       //generate table
            {   
                var row:HTMLTableRowElement=table.insertRow();                  
                var cell:HTMLTableDataCellElement=row.insertCell();
                var text:HTMLInputElement=document.createElement("input");
                text.type="text";
                text.style.textAlign="Center";
                text.style.background="pink";
                text.value=num.toString();
                cell.appendChild(text);

                var cell:HTMLTableDataCellElement=row.insertCell();
                var text:HTMLInputElement=document.createElement("input");
                text.type="text";
                text.style.textAlign="Center";
                text.style.background="mediumorchid";
                text.value="*";
                cell.appendChild(text);

                var cell:HTMLTableDataCellElement=row.insertCell();
                var text:HTMLInputElement=document.createElement("input");
                text.type="text";
                text.style.textAlign="Center";
                text.style.background="pink";
                text.value=count.toString();
                cell.appendChild(text);

                var cell:HTMLTableDataCellElement=row.insertCell();
                var text:HTMLInputElement=document.createElement("input");
                text.type="text";
                text.style.textAlign="Center";
                text.style.background="mediumorchid";
                text.value="=";
                cell.appendChild(text);

                var cell:HTMLTableDataCellElement=row.insertCell();
                var text:HTMLInputElement=document.createElement("input");
                text.type="text";
                text.style.textAlign="Center";
                text.style.background="pink";
                text.value=(count*num).toString();
                cell.appendChild(text);
            }
    }
    else
        alert("Please Enter positive Integer Value... !");

}