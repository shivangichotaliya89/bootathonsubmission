function check_point() {
    //getting the value of the all textbox
    let t11 = document.getElementById("t11");
    let t12 = document.getElementById("t12");
    let t21 = document.getElementById("t21");
    let t22 = document.getElementById("t22");
    let t31 = document.getElementById("t31");
    let t32 = document.getElementById("t32");
    let t41 = document.getElementById("t41");
    let t42 = document.getElementById("t42");
    let ans = document.getElementById("ans"); //for display result
    //convert string value to number
    var x1 = parseFloat(t11.value);
    var y1 = parseFloat(t12.value);
    var x2 = parseFloat(t21.value);
    var y2 = parseFloat(t22.value);
    var x3 = parseFloat(t31.value);
    var y3 = parseFloat(t32.value);
    var x = parseFloat(t41.value);
    var y = parseFloat(t42.value);
    if (t11.value == "" || t12.value == "" || t21.value == "" || t22.value == "" || t31.value == "" || t32.value == "" || t41.value == "" || t42.value == "") //check all the textbox is empty or not
        ans.innerHTML = "<b>Please Enter Value Properly.....! Any one of the field is Empty.<b>";
    else if (isNaN(x1) || isNaN(x2) || isNaN(x3) || isNaN(x) || isNaN(y1) || isNaN(y2) || isNaN(y3) || isNaN(y)) //check the enter coordinates are integers or not
        ans.innerHTML = "<b>Invalid Coordinates....! Coordinate Must be positive integers.<b>";
    else {
        var abc = Math.abs(((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2))) / 2);
        var pab = Math.abs(((x * (y1 - y2) + x1 * (y2 - y) + x2 * (y - y1))) / 2);
        var pbc = Math.abs(((x * (y2 - y3) + x2 * (y3 - y) + x3 * (y - y2))) / 2);
        var pac = Math.abs(((x * (y1 - y3) + x1 * (y3 - y) + x3 * (y - y1))) / 2);
        var sum = pab + pbc + pac;
        if (Math.abs(abc - sum) < 0.0000001) {
            ans.innerHTML = "The Point (" + x + "," + y + ") lies inside the Triangle.";
        }
        else {
            ans.innerHTML = "The Point (" + x + "," + y + ") lies outside the Triangle.";
        }
    }
}
//# sourceMappingURL=triangle_area_cal.js.map