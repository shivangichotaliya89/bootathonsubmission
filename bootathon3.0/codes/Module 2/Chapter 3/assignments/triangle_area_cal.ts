function check_point()
{
    //getting the value of the all textbox
let t11 : HTMLInputElement = <HTMLInputElement>document.getElementById("t11");
let t12 : HTMLInputElement = <HTMLInputElement>document.getElementById("t12");
let t21 : HTMLInputElement = <HTMLInputElement>document.getElementById("t21");
let t22 : HTMLInputElement = <HTMLInputElement>document.getElementById("t22");
let t31 : HTMLInputElement = <HTMLInputElement>document.getElementById("t31");
let t32 : HTMLInputElement = <HTMLInputElement>document.getElementById("t32");

let t41 : HTMLInputElement = <HTMLInputElement>document.getElementById("t41");
let t42 : HTMLInputElement = <HTMLInputElement>document.getElementById("t42");


let ans :HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans"); //for display result

//convert string value to number
var x1:number=parseFloat(t11.value);
var y1:number=parseFloat(t12.value);
var x2:number=parseFloat(t21.value);
var y2:number=parseFloat(t22.value);
var x3:number=parseFloat(t31.value);
var y3:number=parseFloat(t32.value);

var x:number=parseFloat(t41.value);
var y:number=parseFloat(t42.value);

if(t11.value=="" || t12.value=="" || t21.value=="" || t22.value=="" || t31.value=="" || t32.value=="" || t41.value=="" || t42.value=="")//check all the textbox is empty or not
    ans.innerHTML="<b>Please Enter Value Properly.....! Any one of the field is Empty.<b>";
else if(isNaN(x1) || isNaN(x2) || isNaN(x3) || isNaN(x) || isNaN(y1) || isNaN(y2) || isNaN(y3) || isNaN(y) ) //check the enter coordinates are integers or not
    ans.innerHTML="<b>Invalid Coordinates....! Coordinate Must be positive integers.<b>";
else
{
    var abc:number=Math.abs(((x1*(y2-y3)+ x2*(y3-y1) +x3*(y1-y2)))/2);
    var pab:number=Math.abs(((x*(y1-y2) + x1*(y2-y) + x2*(y-y1)))/2);
    var pbc:number=Math.abs(((x*(y2-y3) + x2*(y3-y) + x3*(y-y2)))/2);
    var pac:number=Math.abs(((x*(y1-y3) + x1*(y3-y) + x3*(y-y1)))/2);

    var sum:number=pab+pbc+pac;


    if(Math.abs(abc-sum)<0.0000001)
    {
        ans.innerHTML="The Point ("+ x + "," + y + ") lies inside the Triangle.";
    }
    else
    {
        ans.innerHTML="The Point ("+ x + "," + y + ") lies outside the Triangle.";
    }
}
}