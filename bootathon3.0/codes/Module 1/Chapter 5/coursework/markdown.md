# heading 1
### heading 3

###### heading 6

<h1> heading 1 </h1>
<h6> heading 6 </h2>


*this line is italic*

_this line is italic_

this only word *italic*



**this line is italic**

__this line is bold__

this only word **italic**

> - blockquotes

>This is a blockquotes
>> This is a nested blockquotes


list must start with number 1

1. the list must start with one.
1. watch 

(-), (*),(+)

- this is first type of unordered list
* this is first type of unordered list
+ this is first type of unordered list

[link text](link "title")

[VLabs](http://vlab.iitb.ac.in/vlab/ "click hear")


<support@vlab.co.in>

support@vlab.co.in

to disabble link
`http://www.vlab.co.in`


|Name|Roll No.|Class|
|----|----|----|
|shivu|12|SE|
|pratik|78|BE|



|Name|Roll No.|Class|
|:----|----:|:----:|
|shivu|12|SE|
|pratik|78|BE|

![image name](url or path "title")

![1](1.JPG "hello")
